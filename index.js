import puppeteer from "puppeteer";
import fs, { access } from "fs";

async function saveParseData(resParse, region) {
  const text =
    `Region= ${region}` +
    "\n\n" +
    resParse.result
      .map((result, index) => {
        return (
          `Card ${index + 1}:` +
          "\n" +
          "name= " +
          result.name +
          "\n" +
          "address= " +
          result.address +
          "\n" +
          "price= " +
          result.price +
          "\n" +
          "viewCount= " +
          result.viewCount +
          "\n"
        );
      })
      .join("\n");
  const times = new Date().getTime();
  const screenshot = `${times}_screenshot.jpg`;
  await fs.promises.writeFile(
    screenshot,
    Buffer.from(resParse.screenshot, "base64")
  );
  await fs.promises.writeFile(`${times}_parseResult.txt`, text + "\n", {
    flag: "a",
  });
  console.log("Данные успешно записаны в файл.");
}

async function runner() {
  const resParse = { result: [] };
  const numberOfCardToParse = 20;

  const url = process.argv[2];
  const region = process.argv[3];
  const filter = process.argv[4];

  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ width: 1366, height: 1224 });
  await page.goto(url);

  await setRegion(page, region, filter);
  await page.waitForTimeout(3000);
  const offers = await page.$$("section.a-search-list > div[data-uuid]");
  for (let i = 0; i < numberOfCardToParse && i < offers.length; i++) {
    const result = await runParse(page, i);
    resParse.result.push(result);
  }

  resParse.screenshot = await page.screenshot({
    encoding: "base64",
    fullPage: true,
    timeout: 7000,
  });
  await saveParseData(resParse, region);
  await browser.close();
}

async function setRegion(page, newRegion, filterPrice) {
  await (await page.waitForSelector('span[class*="region-drop"]')).click();
  await page.waitForSelector(
    'div[class*="leveled-select"] select[data-level="2"]'
  );
  const regionBtn = await page.waitForXPath(
    `//option[contains(text(),'${newRegion}')]`,
    { timeout: 2500 }
  );
  regionBtn.click();

  await (
    await page.waitForSelector(
      'div[class*="leveled"]:nth-of-type(2) a[class*="btn"]'
    )
  ).click();

  await page.waitForTimeout(2000);
  const rBtn = await page.waitForSelector('button[type="submit"]');
  await Promise.all([rBtn.click()]);

  try {
    const notesCloseSelector = 'button[class*="tutorial__close"] i';
    const notesClose = await page.waitForSelector(notesCloseSelector, {
      timeout: 5000,
    });
    if (notesClose) {
      await page.click(notesCloseSelector);
    }
  } catch (error) {
    throw error;
  }
  await page.waitForTimeout(2000);
  const optionsBtn = await page.waitForXPath(
    `//span[contains(text(),'${filterPrice}')]`
  );
  optionsBtn.click();
}

async function runParse(page, index) {
  const offers = await page.$$("section.a-search-list > div[data-uuid]");
  const offer = offers[index];
  const name = await offer.$eval('a[class*="a-card__title"]', (element) =>
    element.textContent.trim()
  );
  const address = await offer.$eval('div[class*="card__subtitle"]', (element) =>
    element.textContent.trim()
  );
  const priceElement = await offer.waitForSelector(
    'div[class*="a-card__price"]'
  );
  const price = await priceElement.evaluate(
    (element) => element.textContent.trim().replace(/\s*(&nbsp;)\s*/g, "."),
    priceElement
  );
  const viewCount = await offer.$eval('span[class*="view-count"]', (element) =>
    element.textContent.trim()
  );

  return { name, address, price, viewCount };
}

await runner();
